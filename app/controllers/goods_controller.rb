class GoodsController < ApplicationController

def index
	@goods = Good.all.includes(:revenues)
end

def sales
	render json: { products: 
		Good.all.joins(:revenues).where('revenues.date <= ? AND revenues.date >= ?', params[:to], params[:from])
	}
end

end
