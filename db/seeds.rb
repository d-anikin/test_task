# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Good.delete_all
Revenue.delete_all

require 'roo'

xlsx = Roo::Excelx.new(Rails.root.join('public', 'files', 'seed.xlsx'))

xlsx.each_row_streaming(offset: 1) do |row|
	next if row[0].nil?
  	good = Good.create(name: row[0])
	xlsx.row(1)[1..-1].map.with_index do |val, index|
		next if row[index].to_s.to_f == 0.0
		good.revenues.create(date: val, revenue: row[index].to_s.to_f)
	end
end
